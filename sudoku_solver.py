import time
from timeit import default_timer as timer
import collections


def board_elements(grid):
    
    #convert the board string into dictionary form
    # Eg: 40A57.... into A1:4, A2:0, A3:A, ....
    #A1,A2,A3...are keys and named as boxIDs and 4,0,A,... are each box element values
    chars = []
    for c in grid:
        if c in digits:
            #if box value is given then add that value to dictionary
            chars.append(c)
        #if value is not given then add [123...9] for 9x9 and [123..AB..FG] for 16x16)
        if c == '0':
            chars.append(digits)
    assert len(chars) == (size*size)
    return dict(zip(boxIDs, chars))

def remove(elements):
    
    #Go through all the boxIDs, and whenever there is a box with a value then remove this value from the elements of all its peers.
    #inputs and output are given by dictionary format   
    #obtain all the boxIDs cantain one value(solved value)
    solved_elements = [box for box in elements.keys() if len(elements[box]) == 1]
    for box in solved_elements:
        digit = elements[box]
        for peer in peers[box]:
            elements[peer] = elements[peer].replace(digit, '')
    return elements


def remain_one_element(elements):
    #Go through all the boxes, if a unit (a row, a column , 3x3 peer cell or 4x4 peer cell) 
    #has an eliment only contains relavant digit then give it to that box
    if size==9:
        data='123456789'
    elif size==16:
        data='123456789ABCDEFG'

    for unit in IDList:
        for digit in data:
            #takes the boxes which are contains checking digit in a unit
            re_place = [box for box in unit if digit in elements[box]]
            if len(re_place) == 1:
                #if a box has only the check digit assign it to this box
                elements[re_place[0]] = digit
    return elements

def assign_value(elements, box, value):
    #upade eliments
    elements[box] = value
    return elements


#remove elements using naked twin strategy
def naked_twins(elements):
    # Find all instances of naked twins
    for unit in IDList:
        # Occurrences dict
        #get the number of occurances of digit combinations in the unit
        #Eg : '3':1 digit 3 occurs one time in first row
        #Eg : '469': 1 digit combination of  4,6 & 9 occurs one time in first row
        unit_elements_counter = collections.Counter([elements[box] for box in unit])
       
        for twins, count in unit_elements_counter.items():
            # twins will occur twice in a unit, triples will occur three times, and quads four times
            # Eg: two digit combination occurs two time then it is a twin('25':2)
            # get all twin, triples, quards,....
            if 1 < count == len(twins):
                for box in unit:
                    # for all boxIDs except twins boxIDs in a unit,
                    # remove all potential elements that exist in twins, triples, quads..
                    if elements[box] != twins and set(elements[box]).intersection(set(twins)):
                        for digit in twins:
                            elements = assign_value(elements, box, elements[box].replace(digit, ''))                       
    return elements


def reduce_sudoku(elements):
    stalled = False
    while not stalled:
        #get number of soluved boxIDs
        solved_elements_before = len([box for box in elements.keys() if len(elements[box]) == 1]) 

        #remove the element value from peer boxes whenever the a box contains a value
        elements = remove(elements)

        #assign remain one value for boxex with one available value
        elements = remain_one_element(elements)

        #remove nacked twin elements
        elements = naked_twins(elements)

        solved_elements_after = len([box for box in elements.keys() if len(elements[box]) == 1])
        stalled = solved_elements_before == solved_elements_after
        #if there is a box with no available elements, return False
        if len([box for box in elements.keys() if len(elements[box]) == 0]):
            return False
    #return the solved sudoku
    return elements



def search(elements):

    "Using depth-first search and propagation, create a search tree and solve the sudoku."
    # First, reduce the sudoku using the previous function
    elements = reduce_sudoku(elements)

    #if unable to solve the sudoku return false
    if elements is False:
        return False  ## Failed earlier

    #if sudoku is already solved return solved sudoku
    if all(len(elements[s]) == 1 for s in boxIDs):
        return elements  ## Solved!
    
    #if there are boxes to solve

    # Choose one of the unfilled squares with the fewest possibilities
    min_possibility_box = min([box for box in boxIDs if len(elements[box]) > 1])

    # Now use recursion to solve each one of the resulting sudokus, and if one returns a value (not False), return that answer!
    for digit in elements[min_possibility_box]:
        new_sudoku = elements.copy()
        new_sudoku[min_possibility_box] = digit
        attempt = search(new_sudoku)
        if attempt:
            return attempt

def solve(board_grid):
    elements = board_elements(board_grid)

    #staring the execution time
    start= timer()

    elements = search(elements)

    #stopping the execution time
    end=timer()
    time=end - start

    return elements,time


def draw_board(elements):
    #draw the board as 2D grid
    #elemnts are given as A1:4, A2:0
    if size==9:
      index=3
      row_s='36'
      col_s='CF'

    elif size==16:
      index=4
      row_s='48C'
      col_s='DHL'
    
    #drawing the board
    width = 1 + max(len(elements[s]) for s in boxIDs)
    line = '+'.join(['-' * (width * index)] * index)
    for r in rows:
        print(''.join(elements[r + c].center(width) + ('|' if c in row_s else '')
                      for c in cols))
        if r in col_s: print(line)
    print


def numConverter(num):
        switcher={
                10:'A',
                11:'B',
                12:'C',
                13:'D',
                14:'E',
                15:'F',
                16:'G'
             }
        return switcher.get(num,-1)

def letterConverter(c):
        switcher={
                'A':'10',
                'B':'11',
                'C':'12',
                'D':'13',
                'E':'14',
                'F':'15',
                'G':'16'
             }
        return switcher.get(c,-1)

def make_board(board_grid):
    chars = []
    for c in board_grid:
        if c in digits:
           if c.isalpha():
               #convert A,B,C,D,E,F,G into 10,11,12,13,14,15,16
              converted_c=letterConverter(c)
              chars.append(converted_c)
           else:
             chars.append(c)

        if c == '0':
            chars.append('0')
    
    #checking whether no of elements are 81 or 256
    assert len(chars) == (size*size)
    #assign the value to ech bocID (Eg: A1:4,A2:0,...)
    return dict(zip(boxIDs, chars))

def make_result(result):
    for x in boxIDs:
        if result[x].isalpha():
            #covert A to 10, B to 11,....
            c=letterConverter(result[x])
            result[x]=c
    return result

def cross(stringA, stringB):
    #generate boxIDs IDs like A1, A2,...,B1, B2,......
    #Cross product of elements in string A and elements in string B
    return [s + t for s in stringA for t in stringB]

#print the solved sudoku to a text file
def print_t(result):
    wf = open(output+".txt", "w")

    if size==9:
        divider=9
    elif size==16:
        divider=16

    for i,val in enumerate(boxIDs):
        if i != 0 and i % divider == 0:
            wf.write('\n'+result[val]+' ')
        else:
            wf.write(result[val]+' ')

    wf.close()


val = input("Enter text file name ( Eg: input1): ")
print('selected file: '+val+'.txt\n')

input=val
output= val+'_output'


with open(input+".txt", "r") as file:
    #strore board in 2D board[][] list
    board = [[x for x in line.split()] for line in file]
    file.close()

size=0
#check whether the number of rows and columns are same or not
#if same get the size of the board
if len(board)==len(board[0]):
    size=len(board)


if size>0:   
    boardStr=''
    for i in range(size):
      for j in range(size):
        if size == 9:
            #convert the grid into a string row by row 
            #Eg: 134523..... for 9 x 9
            #Eg: 34A80F..... for 16 x16 here A means 10 F means 15 so on..
            boardStr=boardStr + board[i][j]
        else:
            if int(board[i][j]) > 9:
                # if grid is 16 x 16 convert 10 to A , 11 to B, ..., 16 to G
                letterToNum=numConverter(int(board[i][j]))
                #then convert the grid into a string row by row 
                boardStr = boardStr + letterToNum
            else:
                boardStr=boardStr + board[i][j]

    #naming rows columns and digits in sudoku
    if size==9:
        rows = 'ABCDEFGHI'
        cols = '123456789'
        digits = '123456789'

    elif size==16:
        rows = 'ABCDEFGHIJKLMNOP'
        cols = '123456789ABCDEFG'
        digits = '123456789ABCDEFG'

    #generate boxIDs IDs like A1, A2,...,B1, B2,......
    boxIDs = cross(rows, cols)

    '''finding peers of each box'''
    #Peers of A1 are A2,A3,A4,A5,A6,A7,A8,A9,B1,C1,D1,E1,F1,G1,H1,I1,B2,B3,C2,C3 (in 9 x 9 sudoku)

    #Row units : [A1,A2,..A9],[B1,B2,...],...,[I1,I2,...I9]
    row_units = [cross(r, cols) for r in rows]
    #column units : [A1,B1,...,],[A2,B2,...,A9],...,[B9,...I9]
    column_units = [cross(rows, c) for c in cols]


    if size==9:
        #square unit : [A1,A2,A3,B1,B2,B3,C1,C2,C3],[A4,A5,A6,B4,B5,B6,C4,C5,C6],...
        square_units = [cross(rs, cs) for rs in ('ABC', 'DEF', 'GHI') for cs in ('123', '456', '789')]
    elif size==16:
        square_units = [cross(rs, cs) for rs in ('ABCD', 'EFGH', 'IJKL', 'MNOP') for cs in ('1234', '5678', '9ABC', 'DEFG')]

    
    IDList = row_units + column_units + square_units 
    units = dict((s, [u for u in IDList if s in u]) for s in boxIDs)
    #generate peers of each element
    peers = dict((s, set(sum(units[s], [])) - set([s])) for s in boxIDs)

    
    '''Print the input board'''
    #generate the board
    m_board=make_board(boardStr)
    print('Board\n')
    #print the board
    draw_board(m_board)

    
    '''Solving the sudoku'''
    #calling solve function to solve the sudoku
    result,time1=solve(boardStr)

    if result:
        if size==16:
            result=make_result(result)
        print('\nSolved\n')
        #print the solved sudoku to the console
        draw_board(result)
        print('\nruntime: ',time1,'s')
        #print solved sudoku to a text file
        print_t(result)
    else:
        #sudoku can not solve
        print('\nNo Solution')
        wf = open(output+".txt", "w")
        wf.write('No Solution')
        wf.close()

    print('\n'+input+'.txt result is printed to the '+output+".txt")

else:
    print('Input sudoku grid should be 9x9 or 16x16')


